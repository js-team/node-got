This is a stub types definition for @types/responselike (https://github.com/sindresorhus/responselike#readme).

responselike provides its own type definitions, so you don't need @types/responselike installed!